<?php

return [
    'apiKey' => env('VONAGE_VIDEO_API_KEY'),
    'apiSecret' => env('VONAGE_VIDEO_API_SECRET'),
    'tableUsers' => env('VONAGE_VIDEO_TABLE_USERS', 'users'),
];
