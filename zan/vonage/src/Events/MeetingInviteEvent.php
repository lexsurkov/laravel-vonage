<?php

namespace Zan\Vonage\Events;

/**
 * Class MeetingInviteEvent
 *
 * @package Zan\Vonage\Events
 */
class MeetingInviteEvent extends Event
{
    public int $senderId;
    public int $receiverId;
    public string $sessionId;

    public function __construct($senderId, $receiverId, $sessionId)
    {
        $this->senderId = $senderId;
        $this->receiverId = $receiverId;
        $this->sessionId = $sessionId;
    }
}
