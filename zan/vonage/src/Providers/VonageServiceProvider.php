<?php

namespace Zan\Vonage\Providers;

use Illuminate\Support\ServiceProvider;
use Zan\Vonage\Services\VonageVideo;
use function config_path;

class VonageServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->loadRoutesFrom(__DIR__.'/../../routes/routes.php');
        $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');
        $this->publishes([
            __DIR__.'/../../config/config.php' => config_path('vonage-video.php'),
        ]);
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->mergeConfigFrom(__DIR__.'/../../config/config.php', 'vonage-video');
        $this->app->bind('vonageVideo', function () {
            return new VonageVideo();
        });
    }
}
