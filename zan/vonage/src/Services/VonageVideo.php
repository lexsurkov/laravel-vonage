<?php

namespace Zan\Vonage\Services;

use OpenTok\OpenTok;

/**
 * Class VonageVideo
 *
 * @package Zan\Vonage\Services
 */
class VonageVideo
{
    private $apiKey;
    private $apiSecret;

    private $opentok;

    /**
     * Entry constructor
     */
    public function __construct()
    {
        $this->apiKey = config('vonage-video.apiKey');
        $this->apiSecret = config('vonage-video.apiSecret');

        $this->opentok = new OpenTok($this->apiKey, $this->apiSecret);
    }

    /**
     * Получить sessionId
     *
     * @param array $sessionOptions
     *
     * @return string
     */
    public function getSessionId(array $sessionOptions = []): string
    {
        $session = $this->opentok->createSession($sessionOptions);

        return $session->getSessionId();
    }

    /**
     * Получить токен
     *
     * @param string $sessionId
     * @param array $options
     *
     * @return string
     */
    public function generateToken(string $sessionId, array $options = []): string
    {
        // По умолчанию на 3 часа
        $options['expireTime'] = $options['expireTime'] ?? time()+(3 * 60 * 60);

        return $this->opentok->generateToken($sessionId, $options);
    }

}
