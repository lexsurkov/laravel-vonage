<?php

namespace Zan\Vonage\Actions;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenTok\Role;
use Zan\Vonage\Facades\VonageVideo;
use Zan\Vonage\Models\UserVonageVideo;

class GetSession extends Controller
{
    /**
     * Получить сессию если существует
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @OA\Get(
     *     path="/meetings",
     *     tags={"Онлайн-звонки"},
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        if (auth()->guest()) {
            return response()->json([
                'message' => 'Пользователь не авторизован'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $userVonageVideo = UserVonageVideo::query()->whereUserId(auth()->id())->first();
        if ($userVonageVideo) {
            $token = VonageVideo::generateToken($userVonageVideo->session_id, ['role' => Role::MODERATOR]);

            return response()->json([
                'sessionId' => $userVonageVideo->session_id,
                'token' => $token
            ]);
        }
        return response()->json([]);
    }
}
