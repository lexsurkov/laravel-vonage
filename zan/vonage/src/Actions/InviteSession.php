<?php

namespace Zan\Vonage\Actions;

use App\Http\Controllers\Controller;
use Exception;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use OpenTok\Role;
use Zan\Vonage\Events\MeetingInviteEvent;
use Zan\Vonage\Facades\VonageVideo;
use Zan\Vonage\Models\User;
use Zan\Vonage\Models\UserVonageVideo;

class InviteSession extends Controller
{
    /**
     * Отправить приглашение пользователю
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @OA\Post(
     *     path="/meetings/{sesionId}/invite",
     *     tags={"Онлайн-звонки"},
     * )
     */
    public function __invoke(Request $request, string $sessionId): JsonResponse
    {
        if (auth()->guest()) {
            return response()->json([
                'message' => 'Пользователь не авторизован'
            ], Response::HTTP_UNAUTHORIZED);
        }

        $receiver = User::query()->whereUsername($request->input('username'))->first();
        if (!$receiver) {
            return response()->json([
                'message' => 'Пользователь не найден'
            ], Response::HTTP_NOT_FOUND);
        }

        $userVonageVideo = UserVonageVideo::query()->whereUserId(auth()->id())->first();
        if ($userVonageVideo) {
            event(new MeetingInviteEvent(auth()->id(), $receiver->id, $userVonageVideo->session_id));
        }

        return response()->json([]);
    }
}
