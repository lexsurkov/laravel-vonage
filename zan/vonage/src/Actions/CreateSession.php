<?php

namespace Zan\Vonage\Actions;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenTok\Role;
use Zan\Vonage\Facades\VonageVideo;
use Zan\Vonage\Models\UserVonageVideo;

class CreateSession extends Controller
{
    /**
     * Создание сессии
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @OA\Post(
     *     path="/meetings",
     *     tags={"Онлайн-звонки"},
     * )
     */
    public function __invoke(Request $request): JsonResponse
    {
        if (auth()->guest()) {
            return response()->json([
                'message' => 'Пользователь не авторизован'
            ], response()::HTTP_UNAUTHORIZED);
        }

        $sessionId = VonageVideo::getSessionId();
        $token = VonageVideo::generateToken($sessionId, [
            'role' => Role::MODERATOR,
            'data' => auth()->user()->username
        ]);

        UserVonageVideo::query()->whereUserId(auth()->id())->delete();

        $userVonageVideo = new UserVonageVideo();
        $userVonageVideo->user_id = auth()->id();
        $userVonageVideo->session_id = $sessionId;
        $userVonageVideo->save();

        return response()->json([
          'sessionId' => $sessionId,
          'token' => $token
        ]);
    }
}
