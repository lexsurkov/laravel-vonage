<?php

namespace Zan\Vonage\Actions;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Zan\Vonage\Models\UserVonageVideo;

class CloseSession extends Controller
{
    /**
     * Закрыть сессию
     *
     * @param Request $request
     * @param string $sessionId
     *
     * @return JsonResponse
     * @OA\Delete (
     *     path="/meetings/{sessionId}",
     *     tags={"Онлайн-звонки"},
     * )
     */
    public function __invoke(Request $request, string $sessionId): JsonResponse
    {
        if (auth()->guest()) {
            return response()->json([
                'message' => 'Пользователь не авторизован'
            ], response()::HTTP_UNAUTHORIZED);
        }

        UserVonageVideo::query()->whereSessionId($sessionId)->delete();

        return response()->json([]);
    }
}
