<?php

namespace Zan\Vonage\Actions;

use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use OpenTok\Role;
use Zan\Vonage\Facades\VonageVideo;
use Zan\Vonage\Models\UserVonageVideo;

class GetToken extends Controller
{
    /**
     * Получить токен для сессии
     *
     * @param Request $request
     * @param string $sessionId
     *
     * @return JsonResponse
     * @OA\Get(
     *     path="/meetings/{sessionId}",
     *     tags={"Онлайн-звонки"},
     * )
     */
    public function __invoke(Request $request, string $sessionId): JsonResponse
    {
        if (auth()->guest()) {
            return response()->json([
                'message' => 'Пользователь не авторизован'
            ], response()::HTTP_UNAUTHORIZED);
        }

        $userVonageVideo = UserVonageVideo::query()->whereSessionId($sessionId)->first();
        if ($userVonageVideo) {
            $token = VonageVideo::generateToken($userVonageVideo->session_id, [
                'role' => Role::MODERATOR,
                'data' => auth()->user()->username
            ]);

            return response()->json([
                'sessionId' => $userVonageVideo->session_id,
                'token' => $token
            ]);
        }
        return response()->json([]);
    }
}
