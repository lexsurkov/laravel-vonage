<?php

namespace Zan\Vonage\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Zan\Vonage\Models\UserVonageVideo
 *
 * @property int $user_id
 * @property string $session_id
 * @property mixed|null $created_at
 * @property mixed|null $updated_at
 * @property mixed|null $deleted_at
 *
 * @method static Builder|UserVonageVideo newModelQuery()
 * @method static Builder|UserVonageVideo newQuery()
 * @method static Builder|UserVonageVideo query()
 * @method static Builder|UserVonageVideo whereUserId($value)
 * @method static Builder|UserVonageVideo whereSessionId($value)
 * @mixin Builder
 */
class UserVonageVideo extends Model
{
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'session_id',
    ];
}
