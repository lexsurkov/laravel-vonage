<?php

namespace Zan\Vonage\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

/**
 * Zan\Vonage\Models\User
 *
 * @property int $id
 *
 * @method static Builder|User newModelQuery()
 * @method static Builder|User newQuery()
 * @method static Builder|User query()
 * @mixin Builder
 */
class User extends Model
{
    /**
     * @param array $attributes
     */
    public function __construct(array $attributes = [])
    {
        $this->setTable(config('vonageVideo.tableUsers'));
        parent::__construct($attributes);
    }
}
