<?php

namespace Zan\Vonage\Facades;

use Illuminate\Support\Facades\Facade;

/**
 * @method static string generateToken(string $sessionId, array $options = [])
 * @method static string getSessionId(array $sessionOptions = [])
 * @mixin \Zan\Vonage\Services\VonageVideo
 *
 * @see \Zan\Vonage\Services\VonageVideo
 */
class VonageVideo extends Facade
{
    /**
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'vonageVideo';
    }
}
