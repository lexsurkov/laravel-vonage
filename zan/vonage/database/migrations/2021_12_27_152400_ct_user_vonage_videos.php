<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class CtUserVonageVideos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_vonage_videos', function (Blueprint $table) {
            $table->id();
            if (config('vonage-video.tableUsers')) {
                $table
                    ->foreignId('user_id')
                    ->comment('ID пользователя')->constrained(config('vonage-video.tableUsers'));
            } else {
                $table->unsignedBigInteger('user_id')->comment('ID пользователя');
            }
            $table->string('session_id')->comment('Токен доступа');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_vonage_videos');
    }
}
