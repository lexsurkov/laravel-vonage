<?php

use Illuminate\Support\Facades\Route;
use Zan\Vonage\Actions\CloseSession;
use Zan\Vonage\Actions\CreateSession;
use Zan\Vonage\Actions\GetSession;
use Zan\Vonage\Actions\GetToken;
use Zan\Vonage\Actions\InviteSession;

Route::group(['prefix' => 'api'], function () {
    Route::get('/meetings', GetSession::class);
    Route::post('/meetings', CreateSession::class);
    Route::get('/meetings/{sessionId}', GetToken::class);
    Route::delete('/meetings/{sessionId}', CloseSession::class);
    Route::post('/meetings/{sessionId}/invite', InviteSession::class);
});
